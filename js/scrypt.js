// Get exchange rate currate.ru
const apiURL = 'https://currate.ru/api/';
const MY_API_KEY = '398e99e17cdd64ee3fc78a26e9cef5e2';
const corsURL = 'https://cors-anywhere.herokuapp.com/' // :)

function getCurrency(request, list) {
    var url = new URL(corsURL + apiURL);
    url.searchParams.set('get', 'rates');
    url.searchParams.set('key', MY_API_KEY);
    url.searchParams.set('pairs', list);

    request.open('GET', url);
    request.responseType = 'json';
    request.send();
}

// Initial KZT curency []
function init() {
    var currency_list = ['EURKZT', 'RUBKZT', 'USDKZT', 'BTCKZT']

    var request = new XMLHttpRequest()
    getCurrency(request, currency_list)

    request.onload = function() {
        var data = request.response.data;
        var ul = document.getElementsByTagName("ul")[0];

        for (var i in data) {
            var curen = i.substr(0, 3);
            var li = document.createElement("li");
            li.innerHTML = curen + " -> KZT: " + data[i];
            ul.appendChild(li);
            hash[curen] = data[i];
        }
    }
}

var hash = {}

function setValue(amount) {
    document.getElementById("posthere").value = amount
}

function update() {
    var amount = document.getElementById("amount").value
    var curency = document.getElementById("curency").value
    console.log(amount);
    console.log(curency);
    if (hash[curency] == undefined) {
        console.log("call api");
        
        request = new XMLHttpRequest();
        getCurrency(request, curency+"USD");
        request.onload = function() {
            var data =  request.response.data;
            console.log(data)
            hash[curency] = (hash["USD"]*data[curency+"USD"]).toFixed(4);
            setValue(amount * hash[curency])

            var ul = document.getElementsByTagName("ul")[0];
            var li = document.createElement("li");
            li.innerHTML = curency + " -> KZT: " + hash[curency];
            ul.appendChild(li)
        }
    }
    else setValue(amount*hash[curency])
}

init();

